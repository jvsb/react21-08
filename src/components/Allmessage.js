import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Post from './Post'
import './Allmessage.css'

function Allmessage(){
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        axios.get('https://treinamentoajax.herokuapp.com/messages').then(response =>{
            setPosts(response.data.reverse());
        }) 
    })
    return (
        <div className="posts">
            {posts.map( post => {
                return <Post postName={post.name} postMessage={post.message} />
            })}
        </div>

    )
}
export default Allmessage;