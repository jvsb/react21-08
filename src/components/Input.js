import React, { useState } from 'react';
import axios from 'axios';
import './Input.css'

function Input (){
    const [name, setName] = useState('');
    const [message, setMessage] = useState('');
    
    const HandleType = e => {
        if (e.target.type == "text") {
            setName(e.target.value);
        } else {
            setMessage(e.target.value);
        }
    }  
    const Submit = e => {
        
        setMessage('');
    
        let postBody = {
            "message": {
                "name": name,
                "message": message,
            }
        }
        axios.post('https://treinamentoajax.herokuapp.com/messages', postBody)
            .then( response => {
                console.log("Enviado");
            })
            
    }
    return (
        <form onSubmit={Submit} className="formu">

            <div>
                <label>Seu nome: </label>
                <input type="text" className="Nome" placeholder="Ex: João" onChange={HandleType} value={name} />
            </div>
            <textarea cols="25" rows="10" placeholder="Digite sua mensagem" onChange={HandleType} value={message}></textarea>

            <input type="submit" className="btn" value="Enviar"/>

        </form>
    )

}
export default Input;
