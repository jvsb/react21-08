import React from 'react'
import "./Post.css"

function Post(props) {

    return (
        <div className="post">
            <h2>{props.postName}</h2>
            <p>{props.postMessage}</p>
        </div>
    )
}
export default Post;
