import React from 'react';
import './Header.css';
import {Link} from 'react-router-dom';


function Header () {

  return (
      <nav>
          <ul className="nav-Links">
              <li>
                  <Link className="li-Style" to="/">Home</Link>
              </li>
              <li>
                  <Link className="li-Style" to="/Message">Mensagens</Link>
              </li>
          </ul>
      </nav>
  )
}
export default Header;