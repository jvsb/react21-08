import React from 'react';
import './App.css';
import Header from './Page/Header';
import Message from './Page/Message';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Header/>
      <Switch>
        <Route path="/" exact component = {Home}/>
        <Route path="/Message" component = {Message}>
        <Message/>
        </Route>
      </Switch>
    </div>
    </BrowserRouter>
  );
}
const Home = () => (
  <div>
    <h1>Home Page</h1>
  </div>
);
export default App;
